import math
import time
import sys
import prepare
import generate
import evaluate
import progress

prepare.adjust()
prepare.complete()

sys.stdout.write('Teste Input... ')
sys.stdout.flush()
start_time = time.time()

prepare.check()

print('OK')
print('Laufzeit: %s Sekunden' % math.ceil(time.time() - start_time))
print('--------------------------------------------------')

print('Generiere Möglichkeiten...', end=' ')
start_time = time.time()
print(' ')

generated = generate.start()

print(' ')
print('Laufzeit: %s Sekunden' % math.ceil(time.time() - start_time))
print('--------------------------------------------------')

print('Wähle Möglichkeiten...', end=' ')
start_time = time.time()
print(' ')

runs = 100000

evaluated = evaluate.start(generated, runs)

print(' ')
print('Laufzeit: %s Sekunden' % math.ceil(time.time() - start_time))
print('--------------------------------------------------')

print('Erstelle Verteilungen...', end=' ')
start_time = time.time()
print(' ')

suggest = 100

for i in range(0, suggest):
    choice = evaluate.plot(evaluated[0])
    progress.bar(i+1, suggest)

print(' ')
print('Laufzeit: %s Sekunden' % math.ceil(time.time() - start_time))
print('--------------------------------------------------')
print('Es wurden Textdateien im Ordner "output" erstellt!')
print('--------------------------------------------------')