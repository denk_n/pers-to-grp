import input

def adjust():

    for i_person in range(0, len(input.personen_input)):
        input.personen_gruppen.insert(i_person, [])

        for i_group in range(0, len(input.personen_input[i_person])):
            input.personen_gruppen[i_person].insert(i_group, input.personen_input[i_person][i_group] - 1)

    return True

def complete():

    if not input.gruppen_namen:
        for i in range(0, len(input.gruppen_runden)):
            input.gruppen_namen.append('G' + str(i))

    if not input.raeume_namen:
        for j in range(0, len(input.gruppen_runden)):
            input.raeume_namen.append('R' + str(j))

    if not input.runden_zeiten:
        for k in range(0, input.max_runden):
            input.runden_zeiten.append('XX:XX')

    return True

def check():

    max_choice = len(input.personen_gruppen[0])

    for i_person in range(0, len(input.personen_gruppen)):
        for i_group in range(0, len(input.personen_gruppen[i_person])):

            # teste, ob jedes Angebot im Angebotsarray vorkommt
            if input.personen_gruppen[i_person][i_group] < len(input.gruppen_runden):
                pass
            else:
                return False

            # teste, ob das Personenarray gleich große Arrays enthält
            if len(input.personen_gruppen[i_person]) == max_choice:
               pass
            else:
                return False

    for i_group in range(0, len(input.gruppen_runden)):

        # teste, ob alle Angebote die maximale Rundenzahl nicht überschreiten
        if input.gruppen_runden[i_group] <= input.max_runden:
            pass
        else:
            return False

    # teste, ob die Arrays zu den Angeboten gleich groß sind
    if len(input.gruppen_runden) == len(input.gruppen_namen) and len(input.gruppen_runden) == len(input.raeume_namen):
        pass
    else:
        return False

    # teste, ob die Arrays zu den Runden gleich groß sind
    if len(input.runden_zeiten) == input.max_runden:
        pass
    else:
        return False

    return True