import itertools
import input
import progress

def start():
    max_list = [0] * len(input.gruppen_runden)
    for i_group in range(0, len(input.gruppen_runden)):
        max_list[i_group] = input.max_runden - input.gruppen_runden[i_group]

    off_list = [0] * len(input.gruppen_runden)

    generated = []
    help_sum = []
    i_position_1 = len(input.gruppen_runden) - 1

    while i_position_1 >= 0:

        while off_list[i_position_1] <= max_list[i_position_1]:

            if filter(generate(off_list)) and combine(generate(off_list)) and off_list not in generated:
                generated.append(off_list[:])

            if off_list[i_position_1] < max_list[i_position_1]:
                off_list[i_position_1] += 1
                for i_position_2 in range(i_position_1 + 1, len(input.gruppen_runden)):
                    off_list[i_position_2] = 0
                i_position_1 = len(input.gruppen_runden) - 1
            elif off_list[i_position_1] == max_list[i_position_1]:
                if i_position_1 >= 0:
                    i_position_1 -= 1

            if off_list == max_list:
                break

            help_sum.append(sum(off_list))
            progress.bar(max(help_sum) + 1, sum(max_list))

        if off_list == max_list:
            break

    return generated

def generate(off_list):

    combination = []

    for i_person in range(0, len(input.personen_gruppen)):
        help_list = []

        for i_group in range(0, len(input.personen_gruppen[i_person])):
            group = input.personen_gruppen[i_person][i_group]
            rounds = input.gruppen_runden[group]
            offset = off_list[group]
            help_list.append(list(range(offset, offset + rounds)))

        combination.append(help_list)

    return combination

def filter(combination):

    for i_person in range(0, len(input.personen_gruppen)):
        help_list = []

        for i_group in range(0, len(input.personen_gruppen[i_person])):
            for i_combi in range(0, len(combination[i_person][i_group])):
                help_list.append(combination[i_person][i_group][i_combi])

        if len(set(help_list)) < len(input.personen_gruppen[0]):
            return False

    return True

def combine(combination):

    product = []

    for i_person in range(0, len(input.personen_gruppen)):
        products = itertools.product(*combination[i_person])
        product.append(list(i for i in list(products) if len(set(i)) == len(input.personen_gruppen[0])))

        if len(product[i_person]) < 1:
            return False

    return product