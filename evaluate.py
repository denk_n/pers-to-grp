import input
import random
import generate
import progress
import operator
import math

rand = int

def start(generated, runs, *seed):

    count = 0
    help_list = []
    evaluated = []

    while count < runs:

        if seed is not set:
            seed_def = random.randrange(0, len(generated) - 1)
            combination = generate.combine(generate.generate(generated[seed_def]))
        else:
            combination = generate.combine(generate.generate(generated[seed]))

        count += 1

        distribution = distribute(combination)
        arrangement, sorted_distribution = prepare(distribution)
        evaluation = evaluate(arrangement)

        help_list.append([evaluation[0], evaluation[1]])
        help_list.append(combination)

        if seed is not set:
            help_list.append([seed_def, rand])
        else:
            help_list.append([seed, rand])

        if evaluated == []:
            evaluated.append(help_list[:])
        elif sum(help_list[0]) < sum(evaluated[0][0]):
            evaluated.clear()
            evaluated.append(help_list[:])
            plot(evaluated[0])
        elif sum(help_list[0]) == sum(evaluated[0][0]):
            evaluated.append(help_list[:])

        help_list.clear()
        progress.bar(count, runs)

    return evaluated

def distribute(combination):

    distribution = {}

    for i_person in range(0, len(input.personen_gruppen)):

        distribution[i_person] = {}
        global rand
        rand = random.randrange(0, len(combination[i_person]))

        for i_group in range(0, len(input.personen_gruppen[i_person])):
            group = input.personen_gruppen[i_person][i_group]
            distribution[i_person][group] = combination[i_person][rand][i_group]

    return distribution

def prepare(distribution):

    arrangement = {}

    for i_group in range(1, len(input.gruppen_runden) + 1):
        arrangement[i_group] = {}

        for i_round in range(1, input.max_runden + 1):
            arrangement[i_group][i_round] = 0

    sorted_distribution = {}

    for i_person in range(0, len(input.personen_gruppen)):
        sorted_distribution[i_person] = sorted(distribution[i_person].items(), key=operator.itemgetter(1))

        for i_group in range(0, len(input.personen_gruppen[i_person])):
            group = sorted_distribution[i_person][i_group][0] + 1
            round = sorted_distribution[i_person][i_group][1] + 1
            arrangement[group][round] += 1

    return arrangement, sorted_distribution

def evaluate(arrangement):

    total_round_deviation = 0
    total_member_deviation = 0

    for i_group in range(1, len(input.gruppen_runden) + 1):

        takes_place = 0
        help_sum = 0
        deviation = 0

        for i_round in range(1, input.max_runden + 1):
            help_sum += arrangement[i_group][i_round]

            if arrangement[i_group][i_round]:
                takes_place += 1

        rounds = input.gruppen_runden[i_group - 1]
        spec_val = help_sum / rounds

        for i_round in range(1, input.max_runden + 1):
            deviation += abs(arrangement[i_group][i_round] - spec_val)

        deviation -= (input.max_runden - rounds) * spec_val

        total_round_deviation += rounds - takes_place
        total_member_deviation += deviation

    total_round_deviation = round(total_round_deviation, 1)
    total_member_deviation = round(total_member_deviation, 1)

    return total_round_deviation, total_member_deviation

def plot(choice):

    distribution = distribute(choice[1])
    arrangement, sorted_distribution = prepare(distribution)
    evaluation = evaluate(arrangement)

    filename = 'output//vert__out-' + str(math.ceil(evaluation[0]+evaluation[1])) + '__in-' + str(choice[2][0]) + '-' + str(rand) + '.txt'
    file = open(filename, 'w')

    file.write('Seed: ')
    file.write(str(choice[2][0]) + '\n')
    file.write('Salt: ')
    file.write(str(rand) + '\n')
    file.write('Rundenabweichung: ')
    file.write(str(evaluation[0]) + '\n')
    file.write('Teilnehmerabweichung: ')
    file.write(str(evaluation[1]) + '\n')
    file.write('----------------------------------------\n')

    for k, v in arrangement.items():
        file.write('Angebot ' + str(k) + ': ' + str(v) + '\n')
    file.write('----------------------------------------\n')

    for i_person in range(0, len(input.personen_gruppen)):
        file.write(str('=Tabelle2.A'))
        file.write(str(i_person + 1))
        file.write(str('&" "&Tabelle2.B'))
        file.write(str(i_person + 1))
        file.write(str('&" "&Tabelle2.C'))
        file.write(str(i_person + 1))
        file.write('\n')

        for i_group in range(0, len(input.personen_gruppen[i_person])):
            group = sorted_distribution[i_person][i_group][0]
            file.write(str(input.gruppen_namen[group]))
            file.write(' in ')
            file.write(str(input.raeume_namen[group]))
            file.write(' um ')
            round = sorted_distribution[i_person][i_group][1] + 1
            file.write(str(input.runden_zeiten[round - 1]))
            file.write(' Uhr\n')

        file.write('----------------------------------------\n')

    file.close()

    return True